package bsu.webprog.lab1.controller;

import bsu.webprog.lab1.util.TaskModelGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.OutputStream;

@Controller
public class HomeController {

    private static final String OUTPUT_FILE_PATH = "output_file_path";

    @RequestMapping("/")
    public String indexPage() {
        return "generator";
    }

    @ResponseBody
    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public String generate(@RequestParam int vc, @RequestParam int ac, @RequestParam int tc, @RequestParam int ssc,
                           HttpServletRequest request) throws Exception {
        String res = TaskModelGenerator.generator(vc, ac, tc, ssc);
        File file = new File(System.getProperty("user.home") + File.separator + ".graphgenerator");
        if (!file.exists())
            file.mkdirs();
        file = new File(file, "output.txt");
        FileWriter fileWriter = new FileWriter(file);
        HttpSession session = request.getSession();
        session.setAttribute(OUTPUT_FILE_PATH, file.getAbsolutePath());
        fileWriter.write(res);
        fileWriter.flush();
        return res;
    }

    @RequestMapping(value = "download/txt", method = RequestMethod.GET)
    public void downloadTxt(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        File file = new File((String) session.getAttribute(OUTPUT_FILE_PATH));
        ServletContext context = request.getServletContext();
        FileInputStream inputStream = new FileInputStream(file);

        String mimeType = context.getMimeType(file.getAbsolutePath());
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }
        System.out.println("MIME type: " + mimeType);

        response.setContentType(mimeType);
        response.setContentLength((int) file.length());

        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"",
                file.getName());
        response.setHeader(headerKey, headerValue);

        OutputStream outStream = response.getOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead = -1;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
    }
}
