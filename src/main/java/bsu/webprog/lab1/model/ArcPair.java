package bsu.webprog.lab1.model;

public class ArcPair {

    public int from;
    public int to;
    public boolean additional;

    public ArcPair() {
    }

    public ArcPair(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArcPair arc = (ArcPair) o;

        if (from == arc.from && to == arc.to)
            return true;
        if (from == arc.to && to == arc.from)
            return true;
        return false;
    }

    @Override
    public int hashCode() {
        int result = from;
        result = 31 * result + to;
        return result;
    }

    @Override
    public String toString() {
        return "{" + from + ',' + to + '}' + (additional ? "add" : "");
    }
}
