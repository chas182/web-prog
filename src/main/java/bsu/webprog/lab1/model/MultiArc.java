package bsu.webprog.lab1.model;

public class MultiArc implements Comparable<MultiArc> {

    private Vertex from;
    private Vertex to;
    private boolean[] isOnThread;

    public MultiArc() {
    }

    public MultiArc(Vertex from, Vertex to) {
        this.from = from;
        this.to = to;
    }

    public Vertex getFrom() {
        return from;
    }

    public Vertex getTo() {
        return to;
    }

    public void setThreadCount(int threadCount) {
        this.isOnThread = new boolean[threadCount];
    }


    public void setThread(int threadNumber) {
        this.isOnThread[threadNumber - 1] = true;
    }

    public boolean isOnThread(int threadNumber) {
        return isOnThread[threadNumber - 1];
    }

    public int countThreads() {
        int sum = 0;
        for (boolean anIsOnThread : isOnThread)
            if (anIsOnThread) sum++;
        return sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MultiArc multiArc = (MultiArc) o;

        if (from.equals(multiArc.from) && to.equals(multiArc.to))
            return true;
        if (from.equals(multiArc.to) && to.equals(multiArc.from))
            return true;
        return false;
    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (isOnThread != null)
            for (int i = 0; i < isOnThread.length; ++i)
                sb.append(isOnThread[i] ? 1 : 0).append(i != isOnThread.length - 1 ? ',' : "");

        return "{" + from + " -> " + to + ", [" + sb.toString() + "]}";
    }

    public String simpleString() {
        return "{" + from + "," + to + "}";
    }

    @Override
    public int compareTo(MultiArc m) {
        int cmp = from.getNumber() - m.from.getNumber();
        if (cmp != 0)
            return cmp;
        cmp = to.getNumber() - m.to.getNumber();
        if (cmp != 0)
            return cmp;
        return 0;
    }
}
