package bsu.webprog.lab1.model;

import java.util.List;

public class TaskModel {

    private List<Vertex> vertexes;
    private List<MultiArc> multiArcs;
    private int threadsCount = 0;

    public List<Vertex> getVertexes() {
        return vertexes;
    }

    public void setVertexes(List<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public List<MultiArc> getMultiArcs() {
        return multiArcs;
    }

    public void setMultiArcs(List<MultiArc> multiArcs) {
        this.multiArcs = multiArcs;
    }

    public int getThreadsCount() {
        return threadsCount;
    }

    public void setThreadsCount(int threadsCount) {
        this.threadsCount = threadsCount;
    }
}
