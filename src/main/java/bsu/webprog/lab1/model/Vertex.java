package bsu.webprog.lab1.model;

import java.util.List;


public class Vertex {

    private List<MultiArc> incomingMultiArcs;
    private List<MultiArc> outgoingMultiArcs;
    private int number;

    public Vertex() {
    }

    public Vertex(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public List<MultiArc> getIncomingMultiArcs() {
        return incomingMultiArcs;
    }

    public void setIncomingMultiArcs(List<MultiArc> incomingMultiArcs) {
        this.incomingMultiArcs = incomingMultiArcs;
    }

    public List<MultiArc> getOutgoingMultiArcs() {
        return outgoingMultiArcs;
    }

    public void setOutgoingMultiArcs(List<MultiArc> outgoingMultiArcs) {
        this.outgoingMultiArcs = outgoingMultiArcs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vertex vertex = (Vertex) o;

        return number == vertex.number;
    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + (incomingMultiArcs != null ? incomingMultiArcs.hashCode() : 0);
        result = 31 * result + (outgoingMultiArcs != null ? outgoingMultiArcs.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(number);
    }
}
