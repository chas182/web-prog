package bsu.webprog.lab1.util;

import bsu.webprog.lab1.model.MultiArc;
import bsu.webprog.lab1.model.Vertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alinka on 9/20/2015.
 */
public class GraphUtils {

    public static void generateIncomingAndOutgoingArcs(Vertex vertex, List<MultiArc> multiArcs) {
        List<MultiArc> incomingArcs = new ArrayList<>();
        List<MultiArc> outgoingArcs = new ArrayList<>();
        multiArcs.forEach(multiArc -> {
            if (multiArc.getTo().equals(vertex))
                incomingArcs.add(multiArc);
            if (multiArc.getFrom().equals(vertex))
                outgoingArcs.add(multiArc);
        });
        vertex.setIncomingMultiArcs(incomingArcs);
        vertex.setOutgoingMultiArcs(outgoingArcs);
    }

    public static Vertex getVertex(List<Vertex> vertexes, int number) {
        for (Vertex vertex : vertexes) {
            if (vertex.getNumber() == number)
                return vertex;
        }
        return null;
    }
}
