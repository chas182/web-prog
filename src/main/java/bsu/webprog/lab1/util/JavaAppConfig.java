package bsu.webprog.lab1.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManagerFactoryBean;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class JavaAppConfig {

    @Bean
    public ContentNegotiationManagerFactoryBean getNegotiationManagerFactoryBean() {
        Map<String, MediaType> mediaTypes = new HashMap<String, MediaType>();
        mediaTypes.put("html", MediaType.TEXT_HTML);
        mediaTypes.put("json", MediaType.APPLICATION_JSON);

        ContentNegotiationManagerFactoryBean negotiationManagerFactoryBean = new ContentNegotiationManagerFactoryBean();
        negotiationManagerFactoryBean.setIgnoreAcceptHeader(true);
        negotiationManagerFactoryBean.setDefaultContentType(MediaType.TEXT_HTML);
        negotiationManagerFactoryBean.addMediaTypes(mediaTypes);
        return negotiationManagerFactoryBean;
    }

    @Bean(name = "viewResolver")
    public ContentNegotiatingViewResolver getViewResolver() {
        ContentNegotiatingViewResolver viewResolver = new ContentNegotiatingViewResolver();
        viewResolver.setContentNegotiationManager(getNegotiationManagerFactoryBean().getObject());

        UrlBasedViewResolver urlBasedViewResolver = new UrlBasedViewResolver();
        urlBasedViewResolver.setViewClass(JstlView.class);
        urlBasedViewResolver.setPrefix("/WEB-INF/pages/");
        urlBasedViewResolver.setSuffix(".jsp");
        viewResolver.setViewResolvers(Arrays.<ViewResolver>asList(urlBasedViewResolver));

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
        jsonView.setPrefixJson(true);
        viewResolver.setDefaultViews(Arrays.<View>asList(jsonView));

        viewResolver.setOrder(1);
        return viewResolver;
    }

}
