package bsu.webprog.lab1.util;

import bsu.webprog.lab1.model.ArcPair;
import bsu.webprog.lab1.model.MultiArc;
import bsu.webprog.lab1.model.TaskModel;
import bsu.webprog.lab1.model.Vertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by Alinka on 9/20/2015.
 */
public final class TaskModelGenerator {

    private static final Logger logger = LoggerFactory.getLogger(TaskModelGenerator.class);
    private static final String DIVIDER = "/*--------------*/\n";

    private static String buildStringOutput(TaskModel taskModel, int secondSystemCount, int maxThirdSystemCount) {
        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        sb.append("/*|I|*/").append(taskModel.getVertexes().size()).append('\n');
        sb.append("/*|U|*/").append(taskModel.getMultiArcs().size()).append('\n');
        sb.append("/*|K|*/").append(taskModel.getThreadsCount()).append('\n');

        taskModel.getMultiArcs().forEach(multiArc -> sb.append(multiArc.simpleString()).append('\n'));

        for (int i = 1; i <= taskModel.getThreadsCount(); ++i) {
            sb.append("/*widetilde{U}^").append(i).append("*/\n");
            final int finalI = i;
            taskModel.getMultiArcs().forEach(multiArc -> sb
                    .append("/*").append(multiArc.simpleString()).append("*/")
                    .append(multiArc.isOnThread(finalI) ? 1 : 0).append('\n'));
        }

        sb.append(DIVIDER);
        for (int i = 1; i <= taskModel.getThreadsCount(); ++i) {
            final int threadNumber = i;
            int[] coefs = getRandomInts(taskModel.getVertexes().size());
            final int[] j = {0};
            taskModel.getVertexes().forEach(vertex -> {
                sb.append("/*a_").append(vertex).append('^').append(threadNumber).append("*/").append(coefs[j[0]++]).append('\n');
            });
        }
        sb.append(DIVIDER);

        sb.append("/*q*/").append(secondSystemCount).append('\n');
        for (final int[] i = {1}; i[0] <= secondSystemCount; ++i[0]) {
            taskModel.getMultiArcs().forEach(multiArc -> {
                for (int j = 1; j <= taskModel.getThreadsCount(); ++j) {
                    final int finalI = i[0];
                    int c = r.nextInt(10 + 1);
                    if (c != 0)
                        sb
                                .append("/*lambda_").append(multiArc.getFrom()).append('_').append(multiArc.getTo()).append('^')
                                .append(j).append(finalI).append("*/").append(c).append('\n');
                }
            });
        }
        for (int i = 0; i < secondSystemCount; ++i) {
            int rightPart = r.nextInt(1000 - 300) + 300;
            sb.append("/*alpha_").append(i+1).append("*/").append(rightPart).append('\n');
        }
        sb.append("/*widetilde{U}_0*/\n");
        int arcsCount = 0;
        boolean[] indexes = new boolean[taskModel.getMultiArcs().size()];
        while (arcsCount < maxThirdSystemCount) {
            int i = r.nextInt(taskModel.getMultiArcs().size());
            int threadsCount = taskModel.getMultiArcs().get(i).countThreads();
            if (threadsCount >= 2) {
                arcsCount++;
                indexes[i] = true;
            }
        }
        int p = 0;
        for (MultiArc multiArc : taskModel.getMultiArcs()) {
            int k = 0;
            if (indexes[p++]) {
                k = 1;
            }
            sb.append("/*").append(multiArc.simpleString()).append("*/").append(k);
            int threadsCount = multiArc.countThreads();
            if (k != 0) {
                sb.append(',');
                for (int i = 0; i < threadsCount; ++i)
                    sb.append(i+1).append(i != threadsCount - 1 ? '-': "");
                int c = r.nextInt(20 - 10 + 1) + 10;
                sb.append(',').append(c);
            }
            sb.append('\n');
        };
        return sb.toString();
    }

    private static int[] getRandomInts(int count) {
        int[] rez = new int[count];
        int sum = 0;
        Random r = new Random();
        do {
            sum = 0;
            for (int i = 0; i < count; ++i) {
                rez[i] = r.nextInt(20 - (-20) + 1) + (-20);
                sum += rez[i];
            }
        } while (sum != 0);
        return rez;
    }

    public static String generator(int vertexCount, int arcsCount, int threadsCount, int secondSystemCount) {
        LinkedList<Vertex> vertexesList = new LinkedList<>();
        for (int i = 0; i < vertexCount; i++)
            vertexesList.add(new Vertex(i + 1));

        GraphGenerator[] generators = new GraphGenerator[threadsCount];
        int lastArcs = arcsCount - (vertexCount - 1);
        for (int i = 0; i < generators.length; ++i) {
            generators[i] = new GraphGenerator(vertexesList, vertexCount, arcsCount);
            generators[i].start();
        }
        List<List<ArcPair>> graphs = new LinkedList<>();
        for (GraphGenerator generator : generators) {
            try {
                generator.join();
                graphs.add(generator.getArcs());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        LinkedList<MultiArc> multiArcs = buildMultiGraph(graphs, vertexesList, arcsCount);

        Collections.sort(multiArcs);

        vertexesList.forEach(vertex -> GraphUtils.generateIncomingAndOutgoingArcs(vertex, multiArcs));
        TaskModel taskModel = new TaskModel();
        taskModel.setVertexes(vertexesList);
        taskModel.setMultiArcs(multiArcs);
        taskModel.setThreadsCount(threadsCount);
        return buildStringOutput(taskModel, secondSystemCount, lastArcs);
    }


    private static LinkedList<MultiArc> buildMultiGraph(List<List<ArcPair>> graphs, LinkedList<Vertex> vertexesList, int arcsCount) {
        LinkedList<MultiArc> multiArcs = new LinkedList<>();
        int threadNumber = 0, curArcsCount = 0, vertexCount = vertexesList.size();
        Random r = new Random();
        for (List<ArcPair> graph : graphs) {
            threadNumber++;
            for (ArcPair arc : graph) {
                MultiArc multiArc = new MultiArc(
                        GraphUtils.getVertex(vertexesList, arc.from),
                        GraphUtils.getVertex(vertexesList, arc.to));
                int index = multiArcs.indexOf(multiArc);
                boolean flag = r.nextBoolean();
                if (index != -1) {
                    multiArcs.get(index).setThread(threadNumber);
                } else if (flag && curArcsCount < arcsCount) {
                    multiArc.setThreadCount(graphs.size());
                    multiArc.setThread(threadNumber);
                    multiArcs.add(multiArc);
                    curArcsCount++;
                }
            }
        }

        for (int i = 0; i < graphs.size(); ++i) {
            int arcsOnThread = countArcsOnThread(multiArcs, i + 1);
            int arcsBefore = arcsOnThread;
            if (arcsOnThread < vertexCount - 1) {
                Random random = new Random();
                arcsOnThread = arcsCount == vertexCount - 1 || arcsCount == vertexCount ?
                        arcsCount :
                        random.nextInt(arcsCount - vertexCount + 1) + vertexCount;
                for (int j = 0; j < arcsOnThread - arcsBefore; ++j) {
                    MultiArc multiArc;
                    do {
                        multiArc = getRandomMultiArc(multiArcs, vertexesList);
                        int index = multiArcs.indexOf(multiArc);
                        if (!multiArc.isOnThread(i+1)) {
                            multiArcs.get(index).setThread(i + 1);
                            break;
                        }
                    } while (true);
                }
            }
        }
        return multiArcs;
    }

    private static int countArcsOnThread(List<MultiArc> multiArcs, int threadMumber) {
        int sum = 0;
        for (MultiArc multiArc: multiArcs) {
            if (multiArc.isOnThread(threadMumber)) sum ++;
        }
        return sum;
    }

    private static MultiArc getRandomMultiArc(List<MultiArc> multiArcs, List<Vertex> vertexesList) {
        Random r = new Random();
        while (true) {
            int from = r.nextInt(vertexesList.size()) + 1;
            int to = r.nextInt(vertexesList.size()) + 1;
            MultiArc cur = new MultiArc(
                    GraphUtils.getVertex(vertexesList, from),
                    GraphUtils.getVertex(vertexesList, to));
            for (MultiArc arc : multiArcs) {
                if (arc.equals(cur))
                    return arc;
            }
        }
    }

    private static class GraphGenerator extends Thread {

        private LinkedList<Vertex> vertexesList;
        private int vertexCount;
        private int arcsCount;
        private List<ArcPair> arcs;

        public GraphGenerator(LinkedList<Vertex> vertexesList, int vertexCount, int arcsCount) {
            this.vertexesList = vertexesList;
            this.vertexCount = vertexCount;
            this.arcsCount = arcsCount;
        }

        @Override
        public void run() {
            arcs = new LinkedList<>();
            LinkedList<Vertex> unusedVertexes = new LinkedList<>(vertexesList);
            Random random = new Random();
            while (arcs.size() < arcsCount) {
                ArcPair arc = new ArcPair();
                int index;
                if (unusedVertexes.size() >= 2) {
                    index = random.nextInt(unusedVertexes.size());
                    arc.from = unusedVertexes.get(index).getNumber();
                    index = random.nextInt(unusedVertexes.size());
                    arc.to = unusedVertexes.get(index).getNumber();

                    if (arc.from == arc.to) continue;

                    unusedVertexes.remove(index);
                    if (!arcs.contains(arc)) {
                        arcs.add(arc);
                    }
                } else if (unusedVertexes.size() == 1) {
                    arc.from = unusedVertexes.getFirst().getNumber();
                    index = random.nextInt(vertexCount);
                    arc.to = vertexesList.get(index).getNumber();

                    if (arc.from == arc.to) continue;

                    if (!arcs.contains(arc)) {
                        unusedVertexes.clear();
                        arcs.add(arc);
                    }
                } else {
                    index = random.nextInt(vertexCount);
                    arc.from = vertexesList.get(index).getNumber();
                    index = random.nextInt(vertexCount);
                    arc.to = vertexesList.get(index).getNumber();

                    if (arc.from == arc.to) continue;

                    if (!arcs.contains(arc)) {
                        arcs.add(arc);
                    }
                }
            }
        }

        public List<ArcPair> getArcs() {
            return arcs;
        }
    }
}
