<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Language" content="ru">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<c:url value="/assets/img/favicon.ico"/>">
    <link href="<c:url value="/assets/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/assets/css/bootstrap-theme.min.css"/>" rel="stylesheet"/>
    <script src="<c:url value="/assets/js/jquery-2.0.3.js"/>"></script>
    <script src="<c:url value="/assets/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/assets/js/block.ui.min.js"/>"></script>
    <script src="<c:url value="/assets/js/generator.js"/>"></script>
    <title>Lab 2 :: Web-programming</title>
</head>
<body>

<div class="container" role="main">
    <div class="page-header">
        <h1>Web-Программирование<small>  Лабораторная 2</small></h1>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Введите необходимые данные для генератора</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <form id="inputForm" class="form-horizontal">
                        <div class="form-group">
                            <label for="vertexCountInput" class="col-sm-4 control-label">Кол-во вершин</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="vc" id="vertexCountInput" placeholder="|I|" min="2" autofocus required value="2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="arcCountInput" class="col-sm-4 control-label">Кол-во дуг</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="ac" id="arcCountInput" placeholder="|U|" required value="1" min="1" max="1">
                            </div>
                            <div class="col-sm-3">
                                <div id="error-msg-too-little" class="alert alert-danger hidden" role="alert">Мало дуг.</div>
                                <div id="error-msg-too-much" class="alert alert-danger hidden" role="alert">Много дуг.</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="theadCountInput" class="col-sm-4 control-label">Кол-во потоков</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="tc" id="theadCountInput" placeholder="Threads count" required min="3" max="15" value="3">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="secondSystemCount" class="col-sm-4 control-label">Кол-во ур-ний 2й системы</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="ssc" id="secondSystemCount" placeholder="Threads count" required min="1" value="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button id="generateButton" type="button" class="btn btn-default">Сгенерировать</button>
                            </div>
                        </div>
                        <div id="download-txt" class="form-group hidden">
                            <div class="col-sm-offset-4 col-sm-8">
                                <a href="<c:url value="/download/txt"/>" class="btn btn-default" role="button">Скачать файл</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <textarea id="texResult" class="form-control" rows="20" style="width: 100%" readonly></textarea>
                </div>
            </div>
        </div>
    </div>


</div>

</body>
</html>
