<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Language" content="ru">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<c:url value="/assets/img/favicon.ico"/>">
    <link href="<c:url value="/assets/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/assets/css/bootstrap-theme.min.css"/>" rel="stylesheet"/>
    <script src="<c:url value="/assets/js/jquery-2.0.3.js"/>"></script>
    <script src="<c:url value="/assets/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/assets/js/core.js"/>"></script>
    <title>Lab 1 :: Web-programming</title>
</head>
<body>

<div class="container" role="main">
    <div class="page-header">
        <h1>Web-Программирование<small>  Лабораторная 1</small></h1>
    </div>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">Выбор файла</h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <input id="fileUploadBtn" type="file" name="file" size="50" accept=".txt" style="position:absolute; top:-200px;">

                    <button id="fileUploadBtn-fake" class="btn btn-default" role="button">Отктыть файл</button>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">Исходный файл</h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <div class="col-md-12">
                        <button id="convertToTex" class="btn btn-default" role="button" style="margin-bottom: 20px;">Преобразовать в Tex</button>
                        <textarea id="inputText" class="form-control" rows="20" style="width: 100%"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">Результат</h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <div class="row">
                        <a href="<c:url value="/download/tex"/>" class="btn btn-default" role="button" style="margin-bottom: 20px; margin-left: 15px">Скачать файл</a>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <textarea id="textResult" class="form-control" rows="20" style="width: 100%" readonly></textarea>
                        </div>
                        <div class="col-md-6">
                            <textarea id="texResult" class="form-control" rows="20" style="width: 100%" readonly></textarea>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
