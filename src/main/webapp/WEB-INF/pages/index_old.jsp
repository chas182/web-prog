<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Language" content="ru">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<c:url value="/assets/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/assets/css/bootstrap-theme.min.css"/>" rel="stylesheet"/>
    <script src="<c:url value="/assets/js/jquery-2.0.3.js"/>"></script>
    <script src="<c:url value="/assets/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/assets/js/core_old.js"/>"></script>
</head>
<body>
<div class="container" role="main">
    <div class="jumbotron">
        <h2 id="status">Выберите файл</h2>

        <input id="fileUploadBtn" type="file" name="file" size="50" accept=".txt" style="position:absolute; top:-200px;">

        <button id="fileUploadBtn-fake" class="btn btn-default" role="button">Открыть файл</button>
        <span class="glyphicon glyphicon-arrow-right step-two hidden" aria-hidden="true"></span>
        <button id="convertToTex" class="btn btn-default step-two hidden" role="button">Преобразовать в Tex</button>
        <span class="glyphicon glyphicon-arrow-right step-three hidden" aria-hidden="true"></span>
        <a href="/download/tex" class="btn btn-default step-three hidden" role="button">Скачать файл</a>

        <div class="row" style="margin-top: 20px">
            <div class="col-md-6">
                <textarea id="inputText" class="form-control" rows="25" style="width: 100%"></textarea>
            </div>
            <div class="col-md-6">
                <textarea id="texResult" class="form-control" rows="25" style="width: 100%" readonly></textarea>
            </div>
        </div>
    </div>
</div>
</body>
</html>
