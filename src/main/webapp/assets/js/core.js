(function ($) {

    "use strict"; // jshint ;_;

    $(function() {
        $("#fileUploadBtn-fake").click(function(e) {
            e.preventDefault();
            $("#fileUploadBtn").click();
        });

        $('#fileUploadBtn').change(function (e){
            var $this = $(this),
                fileName = $this.val();
            if (fileName !== "") {
                var files = e.target.files; // FileList object
                var f = files[0];
                var reader = new FileReader();

                reader.onload = (function(theFile) {
                    return function(e) {
                        $("#inputText").val(e.target.result);
                        $("#collapseOne").collapse('hide');
                        $("#collapseTwo").collapse('show');
                    };
                })(f);
                reader.readAsText(f);
            }
        });

        $("#convertToTex").click(function(e) {
            e.preventDefault();

            $.ajax({
                url: "/upload/input",
                type: "PUT",
                data: $("#inputText").val(),
                cashed: false,
                beforeSend: function() {
                    $("#collapseTwo").collapse('hide');
                    $("#textResult").val($("#inputText").val());
                },
                success: function(texString) {
                    $('#texResult').val(texString);
                    $("#collapseThree").collapse('show');
                }
            });
        });

        $("#downloadTex").click(function(e) {
            e.preventDefault();
            $.ajax({
                url: "/download/tex",
                type: "POST",
                data: $("#texResult").val(),
                cashed: false
            });
        });
    });

})(window.jQuery);

