(function ($) {

    "use strict"; // jshint ;_;

    $(function() {
        $("#fileUploadBtn-fake").click(function(e) {
            e.preventDefault();
            $("#fileUploadBtn").click();
        });

        $('#fileUploadBtn').change(function (e){
            var $this = $(this),
                fileName = $this.val();
            if (fileName !== "") {
                var files = e.target.files; // FileList object
                var f = files[0];
                var reader = new FileReader();

                reader.onload = (function(theFile) {
                    return function(e) {
                        $("#inputText").val(e.target.result);
                        $(".step-two").removeClass('hidden');
                        $("#status").text("Отредактируйте и/или преобразуйте в Tex");
                    };
                })(f);
                reader.readAsText(f);
            }
        });

        $("#convertToTex").click(function(e) {
            e.preventDefault();

            $.ajax({
                url: "/upload/input",
                type: "PUT",
                data: $("#inputText").val(),
                cashed: false,
                success: function(texString) {
                    $("#status").text("Отредактируйте и/или преобразуйте в Tex");
                    $(".step-three").removeClass('hidden');
                    $('#texResult').val(texString);
                }
            });
        });

        $("#downloadTex").click(function(e) {
            e.preventDefault();
            $.ajax({
                url: "/download/tex",
                type: "POST",
                data: $("#texResult").val(),
                cashed: false
            });
        });
    });

})(window.jQuery);

