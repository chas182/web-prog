(function ($) {
    function blockUI() {
        $.blockUI({message:'<img src="/assets/img/block.gif">', css: {border: 'none', backgroundColor: 'transparent'}});
    }
    function unblockUI() {
        $.unblockUI();
    }

    "use strict"; // jshint ;_;
    var error = false;
    $(function() {
        $('#vertexCountInput').change(function(e) {
            var vertexCount = $(this).val(),
                maxVertex = vertexCount*(vertexCount-1)/ 2,
                minVertex = vertexCount - 1;
            var $input = $('#arcCountInput');
            $input.attr('min', minVertex);
            $input.attr('max', maxVertex);
            if ($input.val() == "" || $input.val() < minVertex)
                $input.val(vertexCount-1);
            else if ($input.val() > maxVertex) {
                $input.val(maxVertex);
            }
        });
        $('#arcCountInput').change(function(e) {
            var arcsCount = $(this).val(),
                vertexCount = $('#vertexCountInput').val(),
                maxVertex = vertexCount*(vertexCount-1)/ 2,
                minVertex = vertexCount - 1,
                $errorTooMuch = $('#error-msg-too-much'),
                $errorTooLittle = $('#error-msg-too-little'),
                $btn = $('#generateButton');
            $errorTooLittle.addClass('hidden');
            $errorTooMuch.addClass('hidden');
            $btn.enable();
            if (arcsCount < minVertex) {
                $errorTooLittle.removeClass('hidden');
                $btn.disable();
            } else if (arcsCount > maxVertex) {
                $errorTooMuch.removeClass('hidden');
                $btn.disable();
            }
        });

        $("#generateButton").click(function(e) {
            var $form = $('#inputForm');
            $.ajax({
                url: "/generate",
                type: "POST",
                data: $form.serialize(),
                cashed: false,
                beforeSend: function() {
                    blockUI();
                    $("#collapseTwo").collapse('hide');
                    $("#textResult").val($("#inputText").val());
                },
                success: function(texString) {
                    unblockUI();
                    $('#texResult').val(texString);
                    $("#download-txt").removeClass('hidden');
                }
            });
        });

    });

})(window.jQuery);

